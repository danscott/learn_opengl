﻿#pragma once
#include <glad/glad.h>
#include <string>
#include <glm/mat4x4.hpp>

class Shader
{
public:
	unsigned int id;

	Shader(const GLchar* vertex_path, const GLchar* fragment_path);

	void use() const;

	void set_bool(const std::string &name, bool value) const;
	void set_int(const std::string &name, int value) const;
	void set_float(const std::string &name, float value) const;
	void set_mat4(const std::string &name, const glm::mat4 &mat) const;
};
