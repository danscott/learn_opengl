#version 330 core
out vec4 frag_color;
  
in vec3 our_color;
in vec2 tex_coord;

uniform sampler2D texture_1;
uniform sampler2D texture_2;

uniform float mix_amount;

void main()
{
    frag_color = mix(texture(texture_1, tex_coord), texture(texture_2, vec2(tex_coord.x, -tex_coord.y)), mix_amount);
}